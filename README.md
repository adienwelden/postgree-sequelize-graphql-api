# APP Servidor

- API crud de albuns e faixas musicais

## Documentação API

- Swagger API em /api-docs

## Tecnologias

- Node, Babel
- Webpack 4
- Express 4
- Sequelize 4
- Apollo 2
- GraphQL 14

## Implantação

1. create system user:

   - `sudo adduser <user>`

2. create db user, with same name and pass:

   - `sudo -u postgres createuser --interactive`
   - `sudo -u postgres psql`
   - `\password <user>`
   - `\q`

3. test user connection

   - `sudo -u <user> psql -d postgres`

4. create `<proj_root>/config/secret.js` with content:

```
    module.exports = {
        DEV_DATABASE_USER: '<user>',
        DEV_DATABASE_PASSWORD: '<pass>',
        DEV_DATABASE_NAME: '<dbname>',

        TEST_DATABASE_USER: '<user>',
        TEST_DATABASE_PASSWORD: '<pass>',
        TEST_DATABASE_NAME: '<dbname>',

        PROD_DATABASE_USER: '<user>',
        PROD_DATABASE_PASSWORD: '<pass>',
        PROD_DATABASE_NAME: '<dbname>',
    };
```

5. Install App

   - `npm install`

6. set database:

   - `npx sequelize db:create`
   - `npx sequelize db:migrate`

7. Start App
   - `npm start`

## Estrutura

```
+-- <proj_root>/
|	+-- config/
|	|	+-- database.js
|	|	+-- secret.js
|	+-- db/
|	|	+-- migrate/
|	|	|	+-- <migrate files>
|	|	+-- seeds/
|	|	|	+-- <seed files>
|	+-- src/
|	|	+-- graphql/
|	|	|	+-- resolvers.js
|	|	|	+-- schema.js
|	|	+-- models/
|	|	|	+-- album.js
|	|	|	+-- index.js
|	|	|	+-- track.js
|	|	+-- server.js
|	|	+-- swagger.js
|	|	+-- test.js
|	+-- .gitignore
|	+-- .sequelizerc
|	+-- index.js
|	+-- package-lock.json
|	+-- package.json
|	+-- README.md
|	+-- webpack.config.js
```
## Funções
