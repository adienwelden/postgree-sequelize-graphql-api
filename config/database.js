const secret = require('./secret');

module.exports = {
  "development": {
    "username": secret.DEV_DATABASE_USER,
    "password": secret.DEV_DATABASE_PASSWORD,
    "database": secret.DEV_DATABASE_NAME,
    "host": "127.0.0.1",
    "dialect": "postgres",
    "operatorsAliases": false,
  },
  "test": {
    "username": secret.TEST_DATABASE_USER,
    "password": secret.TEST_DATABASE_PASSWORD,
    "database": secret.TEST_DATABASE_NAME,
    "host": "127.0.0.1",
    "dialect": "postgres",
    "operatorsAliases": false,
  },
  "production": {
    "username": secret.PROD_DATABASE_USER,
    "password": secret.PROD_DATABASE_PASSWORD,
    "database": secret.PROD_DATABASE_NAME,
    "host": "127.0.0.1",
    "dialect": "postgres",
    "operatorsAliases": false,
  }
}
