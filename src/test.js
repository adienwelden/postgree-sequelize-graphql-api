const { Album, Track } = require('./models');

const test = async () => {
    // create album
    const album = await Album.create({ name: 'Led Zeppelin II' });
    // create Track
    const track = await Track.create({
      name: 'Babe Im Gonna Leave You',
      AlbumId: album.get('id')
    });
    // select all
    const albumWithDetails = await Album.findOne({
      include: [
        {
          model: Track,
        }
      ]
    });
    console.log(JSON.stringify(albumWithDetails));
};
  
export default test;