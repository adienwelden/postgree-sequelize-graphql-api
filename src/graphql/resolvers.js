import { Album, Track } from '../models';

const resolvers = {
  Query: {
    albums: (obj, args) => Album.findAll(args),
    album: (obj, { id }) => Album.findById(id),
    tracks: (obj, args) => Track.findAll(args),
    track: (obj, { id }) => Track.findById(id)
  },
  Album: {
    tracks: obj => Track.findAll({ where: { AlbumId: obj.id } })
  },
  Track: {
    album: obj => Album.findOne({ where: { id: obj.AlbumId } }),
  },
};

module.exports = resolvers;