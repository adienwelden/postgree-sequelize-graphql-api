const { gql } = require('apollo-server-express');

const typeDefs = gql`
  type Query {
    albums(limit: Int): [Album]
    album(id: ID!): Album
    tracks(limit: Int): [Track]
    track(id: ID!): Track
  }
  
  type Album {
    id: ID!
    name: String!
    tracks: [Track]
  }
  
  type Track {
    id: ID!
    album: Album
    name: String!
  }
`;

module.exports = typeDefs;