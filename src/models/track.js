'use strict';
module.exports = (sequelize, DataTypes) => {
  const Track = sequelize.define('Track', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {});
  Track.associate = function(models) {
    // associations can be defined here
    this.belongsTo(models.Album);
  };
  return Track;
};