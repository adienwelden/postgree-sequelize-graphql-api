'use strict';
module.exports = (sequelize, DataTypes) => {
  const Album = sequelize.define('Album', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {});
  Album.associate = function(models) {
    // associations can be defined here
    this.hasMany(models.Track);
  };
  return Album;
};