import http from 'http';
import Express from 'express';
import bodyParser from 'body-parser';

import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './swagger.json';

import Sequelize from 'sequelize';

import { ApolloServer } from 'apollo-server-express';
import resolvers from './graphql/resolvers';
import typeDefs from './graphql/schema';

import dbConfig from '../config/database';

const mode = 'development';

const auth = {
  user: dbConfig[mode].username,
  pass: dbConfig[mode].password,
  db: dbConfig[mode].database,
}

const sequelize = new Sequelize(
  auth.db,
  auth.user,
  auth.pass,
  {
    host: 'localhost',
    dialect: 'postgres',
    operatorsAliases: false,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
  }
);

sequelize.authenticate()
  .then(() => {
    console.log('Sequelize: Connection has been established successfully.');
    // test();
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

const app = new Express();

app.use(bodyParser.json({ limit: '20mb' }));
app.use(bodyParser.urlencoded({ limit: '20mb', extended: false }));

app.use((req, res, next) => {
  if(req.url !== '/graphql') {
    console.log("===> new server access at", new Date());
    console.log("url", req.url);
    console.log("params", req.params);
    console.log("query", req.query);
    console.log("body", req.body);
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", " Accept, Authorization, Content-Type, Origin, X-Requested-With");
  }
  next();
}); 

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const server = new ApolloServer({ typeDefs, resolvers });
server.applyMiddleware({ app });

const httpsServer = http.createServer(app);

httpsServer.listen(3333, () => {
  console.log("=============================================================");
  console.log('API rodando na porta: 3333!');
  console.log(`ApolloServer rodando em: ${server.graphqlPath}`);
  console.log('Swagger API em /api-docs');
  console.log("=============================================================");
});
